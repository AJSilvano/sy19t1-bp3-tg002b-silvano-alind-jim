#include <iostream>
#include <string>
#include "Pokemon.h"
#include <time.h>
#include <vector>

using namespace std;

int main()
{
	srand(time(NULL));

	int x = 0;
	int y = 0;
	int currentBattlePokemon = 0;
	bool safeArea = true;
	vector<Pokemon*> pokemonStorage;
	vector<Pokemon*> wildPokemons;
	string trainerName;

	{ 
		//List of Pokemons

		Pokemon *Caterpie = new Pokemon("Caterpie", 65, 65, 2, 17, 0, 64); //Base Exp = 53, B.DMG = 30 | 15
		wildPokemons.push_back(Caterpie);

		Pokemon *Weedle = new Pokemon("Weedle", 60, 60, 3, 22, 0, 74); // Base Exp = 52, B.DMG = 35 | 18
		wildPokemons.push_back(Weedle);

		Pokemon *Pidgey = new Pokemon("Pidgey", 53, 53, 3, 28, 0, 79); // Base Exp = 55, B.DMG = 45 | 23
		wildPokemons.push_back(Pidgey);

		Pokemon *Rattata = new Pokemon("Rattata", 40, 40, 3, 34, 0, 82); // Base Exp = 57, B.DMG = 56 | 28
		wildPokemons.push_back(Rattata);

		Pokemon *Spearow = new Pokemon("Spearow", 46, 46, 5, 33, 0, 70); //Base Exp = 58, B.DMG = 60 | 30
		wildPokemons.push_back(Spearow);

		Pokemon *Ekans = new Pokemon("Ekans", 46, 46, 4, 33, 0, 89); //Base Exp = 62, B.DMG = 60 | 30
		wildPokemons.push_back(Ekans);

		Pokemon *Pikachu = new Pokemon("Pikachu", 46, 46, 3, 34, 0, 118); //Base Exp = 82, B.DMG = 55 | 28
		wildPokemons.push_back(Pikachu);

		Pokemon *Clefairy = new Pokemon("Clefairy", 81, 81, 6, 25, 0, 82); //Base Exp = 68, B.DMG = 45 | 23
		wildPokemons.push_back(Clefairy);

		Pokemon *Vulpix = new Pokemon("Vulpix", 44, 44, 4, 23, 0, 76); //Base Exp = 63, B.DMG = 41 | 21
		wildPokemons.push_back(Vulpix);

		Pokemon *Jigglypuff = new Pokemon("Jigglypuff", 38, 38, 1, 23, 0, 76); //Base Exp= 76, B.DMG = 45 | 23
		wildPokemons.push_back(Jigglypuff);
	
		Pokemon *Oddish = new Pokemon("Oddish", 60, 60, 3, 24, 0, 113); //Base Exp = 78, B.DMG = 39 | 20
		wildPokemons.push_back(Oddish);

		Pokemon *Diglett = new Pokemon("Diglett", 12, 12, 2, 19, 0, 97); //Base Exp = 81, B.DMG = 33 | 17
		wildPokemons.push_back(Diglett);

		Pokemon *Mankey = new Pokemon("Mankey", 53, 53, 5, 22, 0, 107); //Base Exp = 74, B.DMG = 35 | 18
		wildPokemons.push_back(Mankey);

		Pokemon *Machop = new Pokemon("Machop", 80, 80, 7, 18, 0, 88); //Base Exp = 88, B.DMG = 35 | 18
		wildPokemons.push_back(Machop);

		Pokemon *Bellsprout = new Pokemon("Bellsprout", 56, 56, 2, 21, 0, 101); //Base Exp = 84, B.DMG = 37 | 19
		wildPokemons.push_back(Bellsprout);
	}

	cout << "Hello! Welcome to the world of Pokemon!" << endl;
	system("pause");
	system("cls");

	cout << "My name is Professor Oak. All right. What's your name? ";
	cin >> trainerName;
	system("cls");

	cout << "I have three(3) Pokemons here." << endl;
	cout << "You can only choose one! Go on " <<trainerName << ", pick one!" << endl;
	cout << endl;
	
	cout << "1 - Bulbasaur               Grass Type               Level 5" << endl;;
	cout << "2 - Charmander              Fire Type                Level 5" << endl;
	cout << "3 - Squirtle                Water Type               Level 5" << endl;

	cout << endl;
	cout << "Please input your desired Pokemon: ";

	int pokemonChoice = 0;
	cin >> pokemonChoice;
	cout << endl;

	if (pokemonChoice == 1)
	{
		Pokemon *Bulbasaur = new Pokemon ("Bulbasaur", 79, 79, 5, 28, 0, 113);
		pokemonStorage.push_back(Bulbasaur);
		cout << "You choose the grass type pokemon, Bulbasaur!" << endl;
	}

	else if (pokemonChoice == 2)
	{
		Pokemon *Charmander = new Pokemon ("Charmander", 69, 69, 5, 28, 0, 113);
		pokemonStorage.push_back(Charmander);
		cout << "You choose the fire type pokemon, Charmander!" << endl;
	}

	else if (pokemonChoice == 3)
	{
		Pokemon *Squirtle = new Pokemon ("Squirtle", 78, 78, 5, 27, 0, 113);
		pokemonStorage.push_back(Squirtle);
		cout << "You choose the water type pokemon, Squirtle!" << endl;
	}

	system("pause");
	system("cls");
	cout << "Your journey begins here! Trainer " << trainerName << "!" << endl;
	system("pause");
	system("cls");

	int trainerChoices = 0;
	while (true)
	{
		if (safeArea == true)
		{
			cout << "What would you like to do?" << endl;
			cout << "[1] - Move          [2] - Pokemons          [3] - Pokemon Center" << endl;

			cin >> trainerChoices;
		}

		else if (safeArea == false)
		{
			cout << "What would you like to do?" << endl;
			cout << "[1] - Move          [2] - Pokemons" << endl;

			cin >> trainerChoices;

			int pokemonEncounterChance = rand() % 100 + 1;
			int pokemonRandomChance = rand() % wildPokemons.size();

			if (pokemonEncounterChance >= 50)
			{
				cout << endl;
				cout << "You encounter a wild pokemon!" << endl;
				wildPokemons[pokemonRandomChance]->healPokemon();
				wildPokemons[pokemonRandomChance]->displayStats();

				system("pause");
				system("cls");

				cout << endl;

				while (true)
				{
					cout << "What would you like to do?" << endl;
					cout << "1 - Battle          2 - Catch          3 - Run away" << endl;

					int trainerChoice = 0;
					cin >> trainerChoice;

					if (trainerChoice == 1)
					{
						int i = 0;

						if (pokemonStorage[currentBattlePokemon]->pokemonIsDead())
						{
							system("cls");
							cout << "You don't have any pokemons left to attack!" << endl;
							system("pause");
							cout << "Run to the nearest Pokemon Center!" << endl;
							system("pause");
							break;
						}

						pokemonStorage[currentBattlePokemon]->attack(wildPokemons[pokemonRandomChance]);

						if (wildPokemons[pokemonRandomChance]->pokemonIsDead())
						{
							system("cls");
							cout << "You won against a wild Pokemon" << endl;
							system("pause");
							cout << endl;
							cout << "Your pokemon received exp!" << endl;
							pokemonStorage[currentBattlePokemon]->pokemonGainExp();
							system("pause");
							cout << endl;
							pokemonStorage[currentBattlePokemon]->displayStats();
							system("pause");

							pokemonStorage[currentBattlePokemon]->pokemonLevelUp();
							break;
						}

						cout << endl;
						wildPokemons[pokemonRandomChance]->attack(pokemonStorage[currentBattlePokemon]);
						system("cls");

						if (pokemonStorage[currentBattlePokemon]->pokemonIsDead())
						{
							system("cls");
							cout << "Your pokemon fainted!" << endl; //error starts here
							system("pause");

							for (int i = 0; i < pokemonStorage.size(); i++)
							{
								if (pokemonStorage[currentBattlePokemon]->pokemonIsDead())
								{
									currentBattlePokemon++;

									if (currentBattlePokemon >= pokemonStorage.size())
									{
										cout << "You don't have any Pokemons left! All of your Pokemons fainted!" << endl;
										break;
									}

									cout << "You brought out your next pokemon!" << endl; //error
								}
							}
						}
					}

					else if (trainerChoice == 2)
					{
						int pokemonCatchSuccess = rand() % 100 + 1;

						cout << "You threw a Pokeball!" << endl;
						system("pause");
						cout << "." << endl;
						system("pause");
						cout << ".." << endl;
						system("pause");
						cout << "..." << endl;

						if (pokemonCatchSuccess <= 30)
						{
							cout << "Successfully caught the Pokemon!" << endl;

							wildPokemons[pokemonRandomChance]->displayStats();
							pokemonStorage.push_back(wildPokemons[pokemonRandomChance]);

							system("pause");
							break;
						}
						
						else
						{
							cout << "Pokemon got out of the ball" << endl;
							system("pause");
							system("cls");
						}
					}

					else if (trainerChoice == 3)
					{
						cout << trainerName << " successfully run away from the pokemon!" << endl;
						system("pause");
						break;
					}
				}
			}
		}
		system("cls");

		if (trainerChoices == 1)
		{
			cout << "Where do you want to move?" << endl;
			cout << "[W] - Up          [S] - Down          [A] - Left          [D] - Right" << endl;

			string trainerDirection;
			cin >> trainerDirection;

			if (trainerDirection == "w")
			{
				y = y + 1;
				cout << "You are now at position (" << x << ", " << y << ")" << endl;
			}

			else if (trainerDirection == "s")
			{
				y = y - 1;
				cout << "You are now at position (" << x << ", " << y << ")" << endl;
			}

			else if (trainerDirection == "a")
			{
				x = x - 1;
				cout << "You are now at position (" << x << ", " << y << ")" << endl;
			}

			else if (trainerDirection == "d")
			{
				x = x + 1;
				cout << "You are now at position (" << x << ", " << y << ")" << endl;
			}

			if (x <= 2 && x >= -2 && y <= 2 && y >= -2)
			{
				cout << "You're in Vermilion City" << endl;
				safeArea = true;
			}

			else if (x <= 2 && x >= -2 && y <= 5 && y >= 3)
			{
				cout << "You're in route 7" << endl;
				safeArea = false;
			}

			else if (x <= -3 && x >= -5 && y >= 3 && y <= 5)
			{
				cout << "You're in Veridian City" << endl;
				safeArea = true;
			}

			else if (x >= 3 && x <= 5 && y >= 3 && y <= 5)
			{
				cout << "You're in Lavender Town" << endl;
				safeArea = true;
			}

			else if (x <= 2 && x >= -2 && y <= -3 && y >= -5)
			{
				cout << "You're in route 3" << endl;
				safeArea = false;
			}

			else if (x <= -3 && x >= -5 && y <= -3 && y >= -5)
			{
				cout << "You're in Pallet Town" << endl;
				safeArea = true;
			}

			else if (x >= 3 && x <= 5 && y <= -3 && y >= -5)
			{
				cout << "You're in Safari Zone" << endl;
				safeArea = true;
			}

			else if (x <= -3 && x >= -5 && y <= 2 && y >= -2)
			{
				cout << "You're in route 5" << endl;
				safeArea = false;
			}

			else if (x >= 3 && x <= 5 && y <= 2 && y >= -2)
			{
				cout << "You're in route 2" << endl;
				safeArea = false;
			}

			else
			{
				cout << "You're at the unknown route" << endl;
				safeArea = false;
			}
		}

		else if (trainerChoices == 2)
		{
			cout << "======================" << endl;
			cout << "   List of Pokemons" << endl;
			cout << "======================" << endl;

			for (int i = 0; i < pokemonStorage.size(); i++)
			{
				cout << endl;
				pokemonStorage[i]->displayStats();
			}
		}

		else if (trainerChoices == 3)
		{
			cout << "Welcome to Pokemon Center!" << endl;
			system("pause");
			cout << "Would you like me to heal your Pokemon back to full health? (Y | N)" << endl;

			string trainerHealChoice;
			cin >> trainerHealChoice;
			if (trainerHealChoice == "y")
			{
				cout << "Hold on..." << endl;
				system("pause");
				cout << "..." << endl;
				system("pause");
				system("cls");

				cout << "Your Pokemons are now in perfect health" << endl;
				cout << endl;
				for (int i = 0; i < pokemonStorage.size(); i++)
				{
					pokemonStorage[i]->healPokemon();
					pokemonStorage[i]->displayStats();
					currentBattlePokemon--;
				}

				system("pause");
				cout << "We hope to see you again" << endl;
			}

			else
			{
				cout << "We hope to see you again" << endl;
			}
		}
		
		system("pause");
		system("cls");
	}

	system("pause");
}