#include "Planet.h"
#include "OgreLight.h"

using namespace std;


Planet::Planet(SceneNode* node)
{
	mNode = node;
}

Planet* Planet::createPlanet(SceneManager* sceneManager, float size, ColourValue colour, string planetMaterial)
{
	Planet* newPlanet = new Planet(sceneManager->getRootSceneNode()->createChildSceneNode());
	//SceneNode* mNode = sceneManager->getRootSceneNode()->createChildSceneNode();
	ManualObject* myPlanet = sceneManager->createManualObject();
	myPlanet->begin(planetMaterial, RenderOperation::OT_TRIANGLE_LIST);
	myPlanet->colour(colour);


	const int nRings = 16;
	const int nSegments = 16;
	const float r = size;

	float fDeltaRingAngle = (Math::PI / nRings);
	float fDeltaSegAngle = (2 * Math::PI / nSegments);
	unsigned short wVerticeIndex = 0;

	// Generate the group of rings for the sphere
	for (int ring = 0; ring <= nRings; ring++) {
		float r0 = r * sinf(ring * fDeltaRingAngle);
		float y0 = r * cosf(ring * fDeltaRingAngle);

		// Generate the group of segments for the current ring
		for (int seg = 0; seg <= nSegments; seg++) {
			float x0 = r0 * sinf(seg * fDeltaSegAngle);
			float z0 = r0 * cosf(seg * fDeltaSegAngle);

			// Add one vertex to the strip which makes up the sphere
			myPlanet->position(x0, y0, z0);
			myPlanet->normal(Vector3(x0, y0, z0).normalisedCopy());
			myPlanet->textureCoord((float)seg / (float)nSegments, (float)ring / (float)nRings);

			if (ring != nRings) {
				// each vertex (except the last) has six indicies pointing to it
				myPlanet->index(wVerticeIndex + nSegments + 1);
				myPlanet->index(wVerticeIndex);
				myPlanet->index(wVerticeIndex + nSegments);
				myPlanet->index(wVerticeIndex + nSegments + 1);
				myPlanet->index(wVerticeIndex + 1);
				myPlanet->index(wVerticeIndex);
				wVerticeIndex++;
			}
		}; // end for seg
	} // end for ring

	myPlanet->end();
	//newPlanet->mNode->attachObject(myPlanet);
	//return newPlanet;
	mNode = sceneManager->getRootSceneNode()->createChildSceneNode();
	mNode->attachObject(myPlanet);
	return new Planet(mNode);

}

Planet::~Planet()
{
}

void Planet::update(const FrameEvent& evt)
{
	mNode->rotate(Vector3(0, 1, 0), Radian(Degree(45 * mLocalRotationSpeed * evt.timeSinceLastFrame)));

	if (this->mParent != NULL)
	{
		Degree planetRevolution = Degree(15 * evt.timeSinceLastFrame * this->mRevolutionSpeed);
		Vector3 planetLocation = Vector3::ZERO;

		planetLocation.x = mNode->getPosition().x - mParent->mNode->getPosition().x;
		planetLocation.z = mNode->getPosition().z - mParent->mNode->getPosition().z;

		float OldX = planetLocation.x;
		float OldZ = planetLocation.z;

		float NewX = (OldX * Math::Cos(planetRevolution)) + (OldZ * Math::Sin(planetRevolution));
		float NewZ = (OldX * -Math::Sin(planetRevolution)) + (OldZ * Math::Cos(planetRevolution));

		this->getNode()->setPosition(getParent()->getNode()->getPosition().x + NewX,
			mNode->getPosition().y, getParent()->getNode()->getPosition().z + NewZ);
	}
}

SceneNode* Planet::getNode()
{
	return mNode;
}

void Planet::setParent(Planet* parent)
{
	mParent = parent;
}

Planet* Planet::getParent()
{
	return mParent;
}

void Planet::setLocalRotationSpeed(float speed)
{
	mLocalRotationSpeed = speed;
}

void Planet::setRevolutionSpeed(float speed)
{
	mRevolutionSpeed = speed;
}
