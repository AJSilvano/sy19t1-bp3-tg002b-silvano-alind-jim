#pragma once
#include "Skill.h"
#include "CharacterUnit.h"

class BasicAttack : public Skill
{
public:
	BasicAttack(string name, int mpCost);
	~BasicAttack();

	void skillCast(CharacterUnit *caster, CharacterUnit * target);
};

