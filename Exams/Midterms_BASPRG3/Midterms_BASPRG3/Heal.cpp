#include "Heal.h"



//Heal::Heal()
//{
//}


Heal::Heal(string name, int mpCost) : Skill(name, mpCost)
{
}

Heal::~Heal()
{
}

void Heal::skillCast(CharacterUnit * caster, CharacterUnit * target)
{
	//Code goes here (effect)
	caster->reduceMp(skillMpCost);
	int heal = target->getHp() * 0.3f;
	cout << caster->getName() << " used " << skillName << " on " << target->getName() << endl;
	caster->healUnit(heal);
	cout << caster->getName() << " was healed for " << heal << " HP" << endl;

}
