#pragma once
#include "Skill.h"
#include "CharacterUnit.h"

class Shockwave : public Skill
{
public:
	Shockwave(string name, int mpCost);
	~Shockwave();

	void skillCast(CharacterUnit *caster, CharacterUnit * target);
};

