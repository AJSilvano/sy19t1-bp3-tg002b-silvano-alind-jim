#include "BasicAttack.h"
#include "CharacterUnit.h"



//BasicAttack::BasicAttack()
//{
//}


BasicAttack::BasicAttack(string name, int mpCost) : Skill (name, mpCost)
{
}

BasicAttack::~BasicAttack()
{
}

void BasicAttack::skillCast(CharacterUnit * caster, CharacterUnit * target)
{
	cout << endl;

	int hittingChance = rand() % 100 + 1;
	int hitRate = (caster->getDex() / target->getAgi()) * 100;

	if (hittingChance > hitRate)
	{
		cout << caster->getName() << " used " << skillName << " against " << target->getName() << endl;
		int power = caster->getPow();
		int randomPow = rand() % power + (power * 0.2f);
		int baseDamage = randomPow * 1.0f;
		int bonusDamage = 1.5f; // if attacker is stronger than the defender
		int vitality = target->getVit();
		int damage = (baseDamage - vitality) * bonusDamage;
		target->takeDamage(damage);

		if (damage < 1)
			damage = 4;

		cout << skillName << " dealt " << damage << " damage." << endl;
	}

	else if (hittingChance < hitRate)
	{
		cout << caster->getName() << " missed the attack!" << endl;
	}
}

//Damage
//Damage is computed using this formula:
//damage = (baseDamage of attacker - VIT of defender) * bonusDamage
//baseDamage = Randomed POW * damageCoefficient
//
//Randomed Power has a variance of 20 % of the unit�s Power.
//This means if your POW is 100, Randomed POW should range from 100 - 119. 
//This is because 20 % of 100 is 20. POW determines the lowest base damage a unit can achieve.
//
//Damage Coefficient depends on the action performed(see skills below).
//For example, a skill may inflict 220 % damage.This means damage coefficient is 2.2.
//
//The lowest damage you can deal is 1. Remember that bonus damage is only applicable 
//if the attacker is strong against the defender.Also, bonus damage is always 150 %
//
//Hit Rate
//Hit rate is computed using this formula:
//hit % = (DEX of attacker / AGI of defender) * 100
//
//hit% can never be lower than 20 and higher than 80. This means you need to clamp the result within this range.
//
//You will use hit% to randomize the chance of landing an attack.
