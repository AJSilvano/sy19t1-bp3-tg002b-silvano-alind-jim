#pragma once
#include "Skill.h"
#include "CharacterUnit.h"

class Heal : public Skill
{
public:
	Heal(string name, int mpCost);
	~Heal();

	void skillCast(CharacterUnit *caster, CharacterUnit * target);
};

