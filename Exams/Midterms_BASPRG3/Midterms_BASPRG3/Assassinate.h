#pragma once
#include "Skill.h"
#include "CharacterUnit.h"

class Assassinate : public Skill
{
public:
	Assassinate(string name, int mpCost);
	~Assassinate();

	void skillCast(CharacterUnit *caster, CharacterUnit * target);
};

