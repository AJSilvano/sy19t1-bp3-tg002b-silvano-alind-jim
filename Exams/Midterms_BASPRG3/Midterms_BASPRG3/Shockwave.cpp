#include "Shockwave.h"



//Shockwave::Shockwave()
//{
//}


Shockwave::Shockwave(string name, int mpCost) : Skill(name, mpCost)
{
}

Shockwave::~Shockwave()
{
}

void Shockwave::skillCast(CharacterUnit * caster, CharacterUnit * target)
{
	//Code goes here
	/*caster->reduceMp(skillMpCost);*/
	int power = caster->getPow();
	int randomPow = rand() % power + (power * 0.2f);
	int baseDamage = randomPow * 0.9f;
	int bonusDamage = 1.5f; // if attacker is stronger than the defender
	int vitality = target->getVit();
	int damage = (baseDamage - vitality) * bonusDamage;
	target->takeDamage(damage);

	if (damage < 1)
		damage = 4;

	cout << skillName << " dealt " << damage << " against " << target->getName() << endl;
}
