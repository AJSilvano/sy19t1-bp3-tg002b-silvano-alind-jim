#include <iostream>
#include <string>
#include <time.h>
#include <vector>
#include "CharacterUnit.h"
#include "Shockwave.h"
#include "Assassinate.h"
#include "Heal.h"

using namespace std;

int main()
{
	srand(time(NULL));

	vector<CharacterUnit*> characterUnits;
	vector<CharacterUnit*> blueTeam;
	vector<CharacterUnit*> redTeam;

	//ALLIED UNITS
	CharacterUnit *allyCharacter1 = new CharacterUnit("Warrior", "Irelia", 50, 20, 16, 10, 8, 13, "Blue Team");
	characterUnits.push_back(allyCharacter1);
	allyCharacter1->skillAdd(new Shockwave("Vanguard's Edge", 5));
	blueTeam.push_back(allyCharacter1);

	CharacterUnit *allyCharacter2 = new CharacterUnit("Assassin", "Akali", 35, 15, 20, 8, 9, 15, "Blue Team");
	characterUnits.push_back(allyCharacter2);
	allyCharacter2->skillAdd(new Assassinate("Perfect Execution", 4));
	blueTeam.push_back(allyCharacter2);

	CharacterUnit *allyCharacter3 = new CharacterUnit("Mage", "Janna", 30, 20, 11, 8, 11, 18, "Blue Team");
	characterUnits.push_back(allyCharacter3);
	allyCharacter3->skillAdd(new Heal("Monsoon", 3));
	blueTeam.push_back(allyCharacter3);
	
	//ENEMY (AI) UNITS
	CharacterUnit *enemyCharacter1 = new CharacterUnit("Warrior", "Swain", 50, 20, 16, 10, 8, 13, "Red Team");
	characterUnits.push_back(enemyCharacter1);
	enemyCharacter1->skillAdd(new Shockwave("Demonic Ascension", 5));
	redTeam.push_back(enemyCharacter1);

	CharacterUnit *enemyCharacter2 = new CharacterUnit("Assassin", "Zed", 38, 15, 20, 8, 9, 15, "Red Team");
	characterUnits.push_back(enemyCharacter2);
	enemyCharacter2->skillAdd(new Assassinate("Death Mark", 4));
	redTeam.push_back(enemyCharacter2);

	CharacterUnit *enemyCharacter3 = new CharacterUnit("Mage", "Soraka", 30, 20, 11, 8, 11, 18, "Red Team");
	characterUnits.push_back(enemyCharacter3);
	enemyCharacter3->skillAdd(new Heal("Wish 107.5", 3));
	redTeam.push_back(enemyCharacter3);

	for (int i = 0; i < characterUnits.size(); i++)
	{
		int unitRandomizer = rand() % (characterUnits.size() - i);
		swap(characterUnits[i], characterUnits[unitRandomizer]);
	}

	while(true)
	{
		cout << "====================" << endl;
		cout << "Team: Blue" << endl;
		cout << "====================" << endl;
		for (int i = 0; i < blueTeam.size(); i++)
		{
			blueTeam.at(i)->displayCurrentStatus();
		}

		cout << endl;

		cout << "====================" << endl;
		cout << "Team: Red" << endl;
		cout << "====================" << endl;
		for (int i = 0; i < redTeam.size(); i++)
		{
			redTeam.at(i)->displayCurrentStatus();
		}
	
		cout << endl;

		cout << "========== TURN ORDER ==========" << endl;
		for (int i = 0; i < characterUnits.size(); i++)
		{
			cout << "#" << i+1 << " "  << "[" <<characterUnits.at(i)->getTeamName() << "] "<< characterUnits.at(i)->getName() << endl;
		}
		cout << "========== TURN ORDER ==========" << endl;

		cout << endl;

		cout << "Current Turn: " << characterUnits.at(0)->getName() << endl;

		system("pause");
		system("cls");

		string theClassType = characterUnits.at(0)->getClass();
		string theTeamName = characterUnits.at(0)->getTeamName();

		int allyChoice;
		int randomizer = rand() % redTeam.size();

		//Attacking (Player VS AI)
		if (theClassType == "Warrior" && theTeamName == "Blue Team")
		{
			characterUnits.at(0)->displayAllyStatusWarrior();
			cout << "Please choose an action: ";
			cin >> allyChoice;

			if (allyChoice == 1)
			{
				
				allyCharacter1->attackBasic(redTeam[randomizer]);
			}

			else if (allyChoice == 2)
			{
				allyCharacter1->reduceMp(5);
				for (int i = 0; i < redTeam.size(); i++)
				{
					allyCharacter1->doShockwave(redTeam.at(i));
				}
			}
		}

		else if (theClassType == "Assassin" && theTeamName == "Blue Team")
		{
			characterUnits.at(0)->displayAllyStatusAssassin();
			cout << "Please choose an action: ";
			cin >> allyChoice;

			if (allyChoice == 1)
			{
				allyCharacter2->attackBasic(redTeam[randomizer]);
			}

			else if (allyChoice == 2)
			{
				int temp;
				for (int i = 0; i < redTeam.size(); i++)
				{
					for (int j = i; j < redTeam.size(); j++)
					{
						if (redTeam[j]->getHp() <= redTeam[i]->getHp())
						{
							temp = j;
						}
					}
				}

				allyCharacter2->doAssassinate(redTeam.at(temp));
			}
		}

		else if (theClassType == "Mage" && theTeamName == "Blue Team")
		{
			characterUnits.at(0)->displayAllyStatusMage();
			cout << "Please choose an action: ";
			cin >> allyChoice;

			if (allyChoice == 1)
			{
				allyCharacter3->attackBasic(redTeam[randomizer]);
			}

			else if (allyChoice == 2)
			{
				int temp;
				for (int i = 0; i < blueTeam.size(); i++)
				{
					for (int j = i; j < blueTeam.size(); j++)
					{
						if (blueTeam[j]->getHp() <= blueTeam[i]->getHp())
						{
							temp = j;
						}
					}
				}

				allyCharacter3->doHeal(blueTeam.at(temp));
			}
		}

		int skillRandomAI = rand() % 100 + 1;

		if (theClassType == "Warrior" && theTeamName == "Red Team")
		{
			if (skillRandomAI <= 50)
			{
				enemyCharacter1->attackBasic(blueTeam[randomizer]);
			}

			else if (skillRandomAI > 50)
			{
				enemyCharacter1->reduceMp(5);
				for (int i = 0; i < blueTeam.size(); i++)
				{
					enemyCharacter1->doShockwave(blueTeam.at(i));
				}
			}
		}

		else if (theClassType == "Assassin" && theTeamName == "Red Team")
		{
			if (skillRandomAI <= 50)
			{

				enemyCharacter2->attackBasic(blueTeam[randomizer]);
			}

			else if (skillRandomAI > 50)
			{
				int temp;
				for (int i = 0; i < blueTeam.size(); i++)
				{
					for (int j = i; j < blueTeam.size(); j++)
					{
						if (blueTeam[j]->getHp() <= blueTeam[i]->getHp())
						{
							temp = j;
						}
					}
				}

				enemyCharacter2->doAssassinate(blueTeam.at(temp));
			}
		}

		else if (theClassType == "Mage" && theTeamName == "Red Team")
		{
			if (skillRandomAI <= 50)
			{
				enemyCharacter3->attackBasic(blueTeam[randomizer]);
			}

			else if (skillRandomAI > 50)
			{
				int temp;
				for (int i = 0; i < redTeam.size(); i++)
				{
					for (int j = i; j < redTeam.size(); j++)
					{
						if (redTeam[j]->getHp() <= redTeam[i]->getHp())
						{
							temp = j;
						}
					}
				}

				enemyCharacter3->doHeal(redTeam.at(temp));
			}
		}

		for (int i = 0; i < characterUnits.size(); i++) //To remove from turn order
		{
			if (characterUnits.at(i)->getHp() <= 0)
			{
				characterUnits.erase(characterUnits.begin() + i);
			}
		}

		for (int i = 0; i < blueTeam.size(); i++) //Player checker (if dead)
		{
			if (blueTeam.at(i)->getHp() <= 0)
			{
				blueTeam.erase(blueTeam.begin() + i);
			}
		}

		for (int i = 0; i < redTeam.size(); i++) //AI checker (if dead)
		{
			if (redTeam.at(i)->getHp() <= 0)
			{
				redTeam.erase(redTeam.begin() + i);
			}
		}

		characterUnits.push_back(characterUnits.at(0));
		characterUnits.erase(characterUnits.begin());

		if (blueTeam.empty() == true || redTeam.empty() == true)
		{
			if (redTeam.empty() == true)
			{
				cout << "You lost the game! Try again next time!" << endl;
				system("pause");
				break;
			}

			else
			{
				cout << "You won the game! Congrats! GG!" << endl;
				system("pause");
				break;
			}	
		}

		system("pause");
		system("cls");
	}

	cout << "Thanks for playing!" << endl;
	system("pause");
}