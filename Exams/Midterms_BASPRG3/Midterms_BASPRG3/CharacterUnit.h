#pragma once
#include <iostream>
#include <string>
#include <vector>

using namespace std;

class Skill;

class CharacterUnit
{
public:
	CharacterUnit(string characterClass, string name, int hp, int mp, int pow, int vit, int dex, int agi, string teamName);
	~CharacterUnit();

	void displayCurrentStatus();
	void displayAllyStatusWarrior();
	void displayAllyStatusAssassin();
	void displayAllyStatusMage();
	void displayTeamName();
	void skillAdd(Skill* skill);
	void attackBasic(CharacterUnit *target);
	void doShockwave(CharacterUnit *target);
	void doAssassinate(CharacterUnit *target);
	void doHeal(CharacterUnit *target);
	void takeDamage(int value);
	void reduceMp(int value);
	bool canCast(Skill *skill);
	bool isAlive();
	void healUnit(int value);


	string getClass();
	string getName();
	int getHp();
	int getMp();
	int getPow();
	int getVit();
	int getDex();
	int getAgi();
	string getTeamName();

private:
	string classType;
	string unitName;
	int unitHp;
	int unitMp;
	int unitPower;
	int unitVitality;
	int unitDexterity;
	int unitAgility;
	string unitTeamName;
	vector<Skill*> mSkills;
};

