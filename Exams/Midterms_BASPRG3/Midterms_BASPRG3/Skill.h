#pragma once
#include <string>
#include "CharacterUnit.h"

using namespace std;

class Skill
{
public:
	Skill(string name, int mpCost);
	~Skill();

	virtual void skillCast(CharacterUnit *caster, CharacterUnit * target);

	string getName();
	int getMpCost();

protected:
	string skillName;
	int skillMpCost;
};