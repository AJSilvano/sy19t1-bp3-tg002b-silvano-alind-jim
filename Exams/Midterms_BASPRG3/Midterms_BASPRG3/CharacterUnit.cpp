#include "CharacterUnit.h"
#include "Skill.h"
#include "BasicAttack.h"
#include "Assassinate.h"
#include "Shockwave.h"
#include "Heal.h"

//CharacterUnit::CharacterUnit()
//{
//}

CharacterUnit::CharacterUnit(string characterClass, string name, int hp, int mp, int pow, int vit, int dex, int agi, string teamName)
{
	this->classType = characterClass;
	this->unitName = name;
	this->unitHp = hp;
	this->unitMp = mp;
	this->unitPower = pow;
	this->unitVitality = vit;
	this->unitDexterity = dex;
	this->unitAgility = agi;
	this->unitTeamName = teamName;

	mSkills.push_back(new BasicAttack("Basic Attack", 0));
	mSkills.push_back(new Assassinate("Assassinate", 4));
	mSkills.push_back(new Shockwave("Shockwave", 5));
	mSkills.push_back(new Heal("Heal", 3));
}

CharacterUnit::~CharacterUnit()
{
}

void CharacterUnit::displayCurrentStatus()
{
	if (unitHp > 0)
	{
		cout << this->unitName << " [HP: " << this->unitHp << "]" << endl;
	}

	else
	{
		cout << this->unitName << " [Dead]" << endl;
	}
}

void CharacterUnit::displayAllyStatusWarrior()
{
	cout << "Name: " << this->unitName << endl;
	cout << "Class: " << this->classType << endl;
	cout << "HP: " << this->unitHp << endl;
	cout << "MP: " << this->unitMp << endl;
	cout << "Pow: " << this->unitPower << endl;
	cout << "Vit: " << this->unitVitality << endl;
	cout << "Dex: " << this->unitDexterity << endl;
	cout << "Agi: " << this->unitAgility << endl;

	cout << "Choose an action..." << endl;
	cout << "==============================" << endl;
	cout << endl;
	cout << "	[1] Basic Attack (MP Cost: 0)" << endl;
	cout << "	[2] Vanguard's Edge (MP Cost: 5)" << endl;
	cout << endl;
}

void CharacterUnit::displayAllyStatusAssassin()
{
	cout << "Name: " << this->unitName << endl;
	cout << "Class: " << this->classType << endl;
	cout << "HP: " << this->unitHp << endl;
	cout << "MP: " << this->unitMp << endl;
	cout << "Pow: " << this->unitPower << endl;
	cout << "Vit: " << this->unitVitality << endl;
	cout << "Dex: " << this->unitDexterity << endl;
	cout << "Agi: " << this->unitAgility << endl;

	cout << "Choose an action..." << endl;
	cout << "==============================" << endl;
	cout << endl;
	cout << "	[1] Basic Attack (MP Cost: 0)" << endl;
	cout << "	[2] Perfect Execution (MP Cost: 4)" << endl;
	cout << endl;
}

void CharacterUnit::displayAllyStatusMage()
{
	cout << "Name: " << this->unitName << endl;
	cout << "Class: " << this->classType << endl;
	cout << "HP: " << this->unitHp << endl;
	cout << "MP: " << this->unitMp << endl;
	cout << "Pow: " << this->unitPower << endl;
	cout << "Vit: " << this->unitVitality << endl;
	cout << "Dex: " << this->unitDexterity << endl;
	cout << "Agi: " << this->unitAgility << endl;

	cout << "Choose an action..." << endl;
	cout << "==============================" << endl;
	cout << endl;
	cout << "	[1] Basic Attack (MP Cost: 0)" << endl;
	cout << "	[2] Monsoon (MP Cost: 3)" << endl;
	cout << endl;
}

void CharacterUnit::displayTeamName()
{
	cout << "[" << this->unitTeamName << "]" << endl;
}

void CharacterUnit::skillAdd(Skill * skill)
{
	mSkills.push_back(skill);
}

void CharacterUnit::attackBasic(CharacterUnit * target)
{
	Skill *skillToUse = mSkills[0];
	if (canCast(skillToUse) == true)
	{
		skillToUse->skillCast(this, target);
	}
	else
	{
		cout << "You cannot use " << skillToUse->getName() << "!" << " You insufficient MP!" << endl;
	}
}

void CharacterUnit::doShockwave(CharacterUnit *target)
{
	Skill *skillToUse = mSkills[2];
	if (canCast(skillToUse) == true)
	{
		skillToUse->skillCast(this, target);
	}
	else
	{
		cout << "You cannot use " << skillToUse->getName() << "!" << " You insufficient MP!" << endl;
	}
}

void CharacterUnit::doAssassinate(CharacterUnit * target)
{
	Skill *skillToUse = mSkills[1];
	if (canCast(skillToUse) == true)
	{
		skillToUse->skillCast(this, target);
	}
	else
	{
		cout << "You cannot use " << skillToUse->getName() << "!" << " You insufficient MP!" << endl;
	}
}

void CharacterUnit::doHeal(CharacterUnit * target)
{
	Skill *skillToUse = mSkills[3];
	if (canCast(skillToUse) == true)
	{
		skillToUse->skillCast(this, target);
	}
	else
	{
		cout << "You cannot use " << skillToUse->getName() << "!" << " You insufficient MP!" << endl;
	}
}

void CharacterUnit::takeDamage(int value)
{
	if (value > 0)
	{
		unitHp -= value;

		if (unitHp < 0)
			unitHp = 0;
	}
}

void CharacterUnit::reduceMp(int value)
{
	if (value > 0)
	{
		unitMp -= value;

		if (unitMp < 0)
			unitMp = 0;
	}
}

bool CharacterUnit::canCast(Skill * skill)
{
	if (unitMp > skill->getMpCost())
	{
		return true;
	}
	else
	{
		return false;
	}
}

bool CharacterUnit::isAlive()
{
	if (unitHp > 0)
	{
		return true;
	}
	else 
	{
		return false;
	}
}

void CharacterUnit::healUnit(int value)
{
	this->unitHp += value;
}

string CharacterUnit::getClass()
{
	return this->classType;
}

string CharacterUnit::getName()
{
	return this->unitName;
}

int CharacterUnit::getHp()
{
	return this->unitHp;
}

int CharacterUnit::getMp()
{
	return this->unitMp;
}

int CharacterUnit::getPow()
{
	return this->unitPower;
}

int CharacterUnit::getVit()
{
	return this->unitVitality;
}

int CharacterUnit::getDex()
{
	return this->unitDexterity;
}

int CharacterUnit::getAgi()
{
	return this->unitAgility;
}

string CharacterUnit::getTeamName()
{
	return this->unitTeamName;
}
