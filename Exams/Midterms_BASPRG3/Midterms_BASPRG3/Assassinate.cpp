#include "Assassinate.h"
#include "CharacterUnit.h"



//Assassinate::Assassinate()
//{
//}


Assassinate::Assassinate(string name, int mpCost) : Skill(name, mpCost)
{
}

Assassinate::~Assassinate()
{
}

void Assassinate::skillCast(CharacterUnit * caster, CharacterUnit * target)
{
	//Code goes here (effect)
	caster->reduceMp(skillMpCost);;
	cout << caster->getName() << " used " << skillName << " against " << target->getName() << endl;
	int power = caster->getPow();
	int randomPow = rand() % power + (power * 0.2f);
	int baseDamage = randomPow * 1.8f;
	int bonusDamage = 1.0f; // if attacker is stronger than the defender
	int vitality = target->getVit();
	int damage = (baseDamage - vitality) * bonusDamage;
	target->takeDamage(damage);

	if (damage <= 0)
		damage = 5;

	cout << skillName << " dealt " << damage << " damage." << endl;
}
