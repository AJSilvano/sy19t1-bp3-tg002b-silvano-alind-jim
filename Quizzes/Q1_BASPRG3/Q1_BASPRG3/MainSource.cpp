#include <iostream>
#include <string>
#include <time.h>
#include <vector>
#include "Player.h"
#include "Items.h"

using namespace std;

int main()
{
	srand(time(NULL));

	Player *AJ = new Player(100, 100, 0, 0);

	while (true)
	{
		if (AJ->isAlive() == false)
		{
			break;
		}

		else if (AJ->canPull() == false)
		{
			break;
		}

		else if (AJ->playerWon() == true)
		{
			break;
		}

		AJ->displayStats();

		AJ->pullRandomItem();

		cout << endl;

		system("pause");
		system("cls");
	}

	if (AJ->isAlive() == false)
	{
		cout << "You lost all of your health! You lost the game!" << endl;
		cout << endl;
		AJ->displayStats();
	}

	else if (AJ->canPull() == false)
	{
		cout << "You lost all of your crystals! Better luck next time!" << endl;
		cout << endl;
		AJ->displayStats();
	}

	else if (AJ->playerWon() == true)
	{
		cout << "You won the game! Congratulations! GG!!" << endl;
		cout << endl;
		AJ->displayStats();
	}

	AJ->displayPulledItems();

	system("pause"); 
}