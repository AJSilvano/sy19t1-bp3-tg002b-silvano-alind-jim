#pragma once
#include "Items.h"
class Bomb : public Items
{
public:
	Bomb(string name, int crystalCost);
	~Bomb();

	void pullRandom(Player *thisPlayer);
};

