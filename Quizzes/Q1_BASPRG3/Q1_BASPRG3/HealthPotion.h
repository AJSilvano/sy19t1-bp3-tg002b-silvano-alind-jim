#pragma once
#include "Items.h"
class HealthPotion : public Items
{
public:
	HealthPotion(string name, int crystalCost);
	~HealthPotion();

	void pullRandom(Player *thisPlayer);
};

