#pragma once
#include <string>
#include <iostream>
#include <vector>
#include "Items.h"

using namespace std;

class Items;	
class Player
{
public:
	Player(int hp, int crystals, int rPoints, int pulls);
	~Player();

	void displayStats();
	bool isAlive();
	void pullRandomItem();
	bool canPull();
	bool playerWon();

	void takeDamageBomb(int value);
	void reduceCrystal(int value);
	void heal(int value);
	void getRarityPoints(int value);
	void getCrystals(int value);
	void numberOfPulls(int value);

	void displayPulledItems();
	void itemCounterSSR(int quantity);
	void itemCounterSR(int quantity);
	void itemCounterR(int quantity);
	void itemCounterPotion(int quantity);
	void itemCounterBomb(int quantity);
	void itemCounterCrystals(int quantity);

private:
	int playerHp;
	int playerCrystals;
	int playerRarityPoints;
	int playerPulls;

	int itemPulledSSR;
	int itemPulledSR;
	int itemPulledR;
	int itemPulledPotion;
	int itemPulledBomb;
	int itemPulledCrystals;
	vector<Items*> randomItem;
};

