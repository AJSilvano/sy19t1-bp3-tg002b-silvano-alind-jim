#include "Crystal.h"

Crystal::Crystal(string name, int crystalCost) : Items(name, crystalCost)
{
}

Crystal::~Crystal()
{
}

void Crystal::pullRandom(Player * thisPlayer)
{
	cout << "You pulled Crystals" << endl;
	cout << "You received 15 Crystals" << endl;

	int addCrystal = 15;
	thisPlayer->getCrystals(addCrystal);
	thisPlayer->reduceCrystal(5);
	thisPlayer->numberOfPulls(1);
	thisPlayer->itemCounterCrystals(1);
}
