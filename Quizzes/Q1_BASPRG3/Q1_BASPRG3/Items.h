#pragma once
#include <string>
#include "Player.h"

using namespace std; 

//Base Class for the Items
class Player;
class Items
{
public:
	Items(string name, int crystalCost);
	~Items();

	virtual void pullRandom(Player *thisPlayer);

	string getName();
	int getCrystalCost();
protected:
	string itemName;
	int itemCrystalCost;
};

