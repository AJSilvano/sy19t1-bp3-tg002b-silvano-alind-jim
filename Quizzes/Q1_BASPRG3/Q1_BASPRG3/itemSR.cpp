#include "itemSR.h"

itemSR::itemSR(string name, int crystalCost) : Items(name, crystalCost)
{
}

itemSR::~itemSR()
{
}

void itemSR::pullRandom(Player * thisPlayer)
{
	cout << "You pulled SR" << endl;
	cout << "You received 10 Rarity Points" << endl;

	int rarityPointSR = 10;
	thisPlayer->getRarityPoints(rarityPointSR);
	thisPlayer->reduceCrystal(5);
	thisPlayer->numberOfPulls(1);
	thisPlayer->itemCounterSR(1);
}
