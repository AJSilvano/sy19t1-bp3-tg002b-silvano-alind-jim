#include "itemSSR.h"

itemSSR::itemSSR(string name, int crystalCost) : Items(name, crystalCost)
{
}

itemSSR::~itemSSR()
{
}

void itemSSR::pullRandom(Player * thisPlayer)
{
	cout << "You pulled SSR" << endl;
	cout << "You received 50 Rarity Points!!" << endl;

	int rarityPointSSR = 50;
	thisPlayer->getRarityPoints(rarityPointSSR);
	thisPlayer->reduceCrystal(5);
	thisPlayer->numberOfPulls(1);
	thisPlayer->itemCounterSSR(1);
}
