#include "Items.h"
#include "Player.h"


Items::Items(string name, int crystalCost)
{
	itemName = name;
	itemCrystalCost = crystalCost;
}

Items::~Items()
{
}

void Items::pullRandom(Player * thisPlayer)
{
}

string Items::getName()
{
	return itemName;
}

int Items::getCrystalCost()
{
	return itemCrystalCost;
}
