#include "HealthPotion.h"

HealthPotion::HealthPotion(string name, int crystalCost) : Items(name, crystalCost)
{
}

HealthPotion::~HealthPotion()
{
}

void HealthPotion::pullRandom(Player * thisPlayer)
{
	cout << "You pulled Health Potion" << endl;
	cout << "You are healed for 30 HP" << endl;
	
	int healingPoint = 30;
	thisPlayer->heal(healingPoint);
	thisPlayer->reduceCrystal(5);
	thisPlayer->numberOfPulls(1);
	thisPlayer->itemCounterPotion(1);
}
