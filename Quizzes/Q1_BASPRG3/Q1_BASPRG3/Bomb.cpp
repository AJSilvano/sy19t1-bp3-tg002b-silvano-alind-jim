#include "Bomb.h"

Bomb::Bomb(string name, int crystalCost) : Items(name, crystalCost)
{
}

Bomb::~Bomb()
{
}

void Bomb::pullRandom(Player * thisPlayer)
{
	cout << "You pulled a Bomb!" << endl;
	cout << "You received 25 damage" << endl;

	int damage = 25;
	thisPlayer->takeDamageBomb(damage);
	thisPlayer->reduceCrystal(5);
	thisPlayer->numberOfPulls(1);
	thisPlayer->itemCounterBomb(1);
}
