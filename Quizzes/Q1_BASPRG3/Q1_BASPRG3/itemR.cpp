#include "itemR.h"

itemR::itemR(string name, int crystalCost) : Items(name,crystalCost)
{
}

itemR::~itemR()
{
}

void itemR::pullRandom(Player * thisPlayer)
{
	cout << "You pulled R" << endl;
	cout << "You received 1 Rarity Points" << endl;

	int rarityPointR = 1;
	thisPlayer->getRarityPoints(rarityPointR);
	thisPlayer->reduceCrystal(5);
	thisPlayer->numberOfPulls(1);
	thisPlayer->itemCounterR(1);
}
