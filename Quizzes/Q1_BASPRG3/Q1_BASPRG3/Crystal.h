#pragma once
#include "Items.h"
class Crystal : public Items
{
public:
	Crystal(string name, int crystalCost);
	~Crystal();

	void pullRandom(Player *thisPlayer);
};

