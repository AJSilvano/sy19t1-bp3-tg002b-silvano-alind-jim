#include "Player.h"
#include "Items.h"
#include "itemR.h"
#include "itemSR.h"
#include "itemSSR.h"
#include "HealthPotion.h"
#include "Bomb.h"
#include "Crystal.h"
#include <vector>

Player::Player(int hp, int crystals, int rPoints, int pulls)
{
	playerHp = hp;
	playerCrystals = crystals;
	playerRarityPoints = rPoints;
	playerPulls = pulls;

	randomItem.push_back(new itemR("R", 5));
	randomItem.push_back(new itemSR("SR", 5));
	randomItem.push_back(new itemSSR("SSR", 5));
	randomItem.push_back(new HealthPotion("HealthPotion", 5));
	randomItem.push_back(new Bomb("Bomb", 5));
	randomItem.push_back(new Crystal("Crystal", 5));
}

Player::~Player()
{

}

void Player::displayStats()
{
	cout << "HP: " << playerHp << endl;
	cout << "Crystals: " << playerCrystals << endl;
	cout << "Rarity Points: " << playerRarityPoints << endl;
	cout << "Pulls: " << playerPulls << endl;
	cout << endl;
}

bool Player::isAlive()
{
	if (playerHp > 0)
	{
		return true;
	}
	else
	{
		return false;
	}
}

void Player::pullRandomItem()
{
	int randomizerItem = rand() % 100 + 1;

	
	if (randomizerItem == 1)
	{
		Items *pullingItem = randomItem[2];
		pullingItem->pullRandom(this);
	}

	else if (randomizerItem <= 10 && randomizerItem > 1)
	{
		Items *pullingItem = randomItem[1];
		pullingItem->pullRandom(this);
	}

	else if (randomizerItem <= 25 && randomizerItem > 10)
	{
		Items *pullingItem = randomItem[5];
		pullingItem->pullRandom(this);
	}

	else if (randomizerItem <= 40 && randomizerItem > 25)
	{
		Items *pullingItem = randomItem[3];
		pullingItem->pullRandom(this);
	}

	else if (randomizerItem <= 60 && randomizerItem > 40)
	{
		Items *pullingItem = randomItem[4];
		pullingItem->pullRandom(this);
	}

	else if (randomizerItem <= 100 && randomizerItem > 60)
	{
		Items *pullingItem = randomItem[0];
		pullingItem->pullRandom(this);
	}
	 
}

bool Player::canPull()
{
	if (playerCrystals > 0)
	{
		return true;
	}
	else
	{
		return false;
	}
}

bool Player::playerWon()
{
	if (playerRarityPoints >= 100)
	{
		return true;
	}
	else
	{
		return false;
	}
}

void Player::takeDamageBomb(int value)
{
	if (value > 0)
	{
		playerHp -= value;

		if (playerHp < 0)
			playerHp = 0;
	}
}

void Player::reduceCrystal(int value)
{
	playerCrystals -= value;
}

void Player::heal(int value)
{
	playerHp += value;
}

void Player::getRarityPoints(int value)
{
	playerRarityPoints += value;
}

void Player::getCrystals(int value)
{
	playerCrystals += value;
}

void Player::numberOfPulls(int value)
{
	playerPulls += value;
}

void Player::displayPulledItems()
{
	cout << "Items Pulled:" << endl;
	cout << endl;
	cout << "Health Potion: " << "x"<< itemPulledPotion << endl;
	cout << "Bomb: " << "x" << itemPulledBomb << endl;
	cout << "Crystals: " << "x" << itemPulledCrystals << endl;
	cout << "R: " << "x" << itemPulledR << endl;
	cout << "SR: " << "x" << itemPulledSR << endl;
	cout << "SSR: " << "x" << itemPulledSSR << endl;
	cout << endl;
}

void Player::itemCounterSSR(int quantity)
{
	itemPulledSSR += quantity;
}

void Player::itemCounterSR(int quantity)
{
	itemPulledSR += quantity;
}

void Player::itemCounterR(int quantity)
{
	itemPulledR += quantity;
}

void Player::itemCounterPotion(int quantity)
{
	itemPulledPotion += quantity;
}

void Player::itemCounterBomb(int quantity)
{
	itemPulledBomb += quantity;
}

void Player::itemCounterCrystals(int quantity)
{
	itemPulledCrystals += quantity;
}
