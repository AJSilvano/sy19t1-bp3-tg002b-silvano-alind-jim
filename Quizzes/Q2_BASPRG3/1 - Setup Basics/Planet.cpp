#include "Planet.h"

Planet::Planet(SceneNode * node)
{
	mNode = node;
}

Planet * Planet::createPlanet(SceneManager* sceneManager, float size, ColourValue colour)
{
	Planet* newPlanet = new Planet(sceneManager->getRootSceneNode()->createChildSceneNode());
	//SceneNode* mNode = sceneManager->getRootSceneNode()->createChildSceneNode();
	ManualObject* myPlanet = sceneManager->createManualObject();
	myPlanet->begin("BaseWhiteNoLighting", RenderOperation::OT_TRIANGLE_LIST);
	myPlanet->colour(colour);
	
	//BACK T-1
	myPlanet->position(size, size, -size);
	myPlanet->position(-size, -size, -size);
	myPlanet->position(-size, size, -size);

	//BACK T-2
	myPlanet->position(size, size, -size);
	myPlanet->position(size, -size, -size);
	myPlanet->position(-size, -size, -size);

	//LEFT T-1
	myPlanet->position(-size, size, -size);
	myPlanet->position(-size, -size, -size);
	myPlanet->position(-size, -size, size);

	//LEFT T-2
	myPlanet->position(-size, size, size);
	myPlanet->position(-size, size, -size);
	myPlanet->position(-size, -size, size);

	//RIGHT T-1
	myPlanet->position(size, size, size);
	myPlanet->position(size, -size, size);
	myPlanet->position(size, -size, -size);

	//RIGHT T-2
	myPlanet->position(size, size, size);
	myPlanet->position(size, -size, -size);
	myPlanet->position(size, size, -size);

	//FRONT T-1
	myPlanet->position(-size, size, size);
	myPlanet->position(-size, -size, size);
	myPlanet->position(size, -size, size);

	//FRONT T-2
	myPlanet->position(size, size, size);
	myPlanet->position(-size, size, size);
	myPlanet->position(size, -size, size);

	//TOP T-1
	myPlanet->position(size, size, -size);
	myPlanet->position(-size, size, -size);
	myPlanet->position(-size, size, size);

	//TOP T-2
	myPlanet->position(size, size, -size);
	myPlanet->position(-size, size, size);
	myPlanet->position(size, size, size);

	//BOTTOM T-1
	myPlanet->position(size, -size, size);
	myPlanet->position(-size, -size, size);
	myPlanet->position(-size, -size, -size);

	//BOTTOM T-1
	myPlanet->position(size, -size, size);
	myPlanet->position(-size, -size, -size);
	myPlanet->position(size, -size, -size);

	myPlanet->end();

	//newPlanet->mNode->attachObject(myPlanet);
	//return newPlanet;
	mNode = sceneManager->getRootSceneNode()->createChildSceneNode();
	mNode->attachObject(myPlanet);
	return new Planet(mNode);
	
}

Planet::~Planet()
{
}

void Planet::update(const FrameEvent & evt)
{
	mNode->rotate(Vector3(0, 1, 0), Radian(Degree(45 * mLocalRotationSpeed * evt.timeSinceLastFrame)));
	
	if (this->mParent != NULL)
	{
		Degree planetRevolution = Degree(15 * evt.timeSinceLastFrame * this->mRevolutionSpeed);
		Vector3 planetLocation = Vector3::ZERO; 

		planetLocation.x = mNode->getPosition().x - mParent->mNode->getPosition().x;
		planetLocation.z = mNode->getPosition().z - mParent->mNode->getPosition().z;

		float OldX = planetLocation.x;
		float OldZ = planetLocation.z;

		float NewX = (OldX * Math::Cos(planetRevolution)) + (OldZ * Math::Sin(planetRevolution));
		float NewZ = (OldX * -Math::Sin(planetRevolution)) + (OldZ * Math::Cos(planetRevolution));

		this->getNode()->setPosition(getParent()->getNode()->getPosition().x + NewX,
			mNode->getPosition().y, getParent()->getNode()->getPosition().z + NewZ);
	}
}

SceneNode * Planet::getNode()
{
	return mNode;
}

void Planet::setParent(Planet * parent)
{
	mParent = parent;
}

Planet * Planet::getParent()
{
	return mParent;
}

void Planet::setLocalRotationSpeed(float speed)
{
	mLocalRotationSpeed = speed;
}

void Planet::setRevolutionSpeed(float speed)
{
	mRevolutionSpeed = speed;
}
