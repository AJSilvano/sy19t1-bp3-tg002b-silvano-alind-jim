/*
-----------------------------------------------------------------------------
Filename:    TutorialApplication.cpp
-----------------------------------------------------------------------------

This source file is part of the
   ___                 __    __ _ _    _
  /___\__ _ _ __ ___  / / /\ \ (_) | _(_)
 //  // _` | '__/ _ \ \ \/  \/ / | |/ / |
/ \_// (_| | | |  __/  \  /\  /| |   <| |
\___/ \__, |_|  \___|   \/  \/ |_|_|\_\_|
	  |___/
Tutorial Framework (for Ogre 1.9)
http://www.ogre3d.org/wiki/
-----------------------------------------------------------------------------
*/

#include "TutorialApplication.h"
#include "Planet.h"
#include <OgreManualObject.h>


//---------------------------------------------------------------------------
TutorialApplication::TutorialApplication(void)
{

}
//---------------------------------------------------------------------------
TutorialApplication::~TutorialApplication(void)
{
}

//---------------------------------------------------------------------------
void TutorialApplication::createScene(void)
{
	sun = new Planet(cubeNode);
	sun->createPlanet(mSceneMgr, 10, ColourValue::ColourValue(1, 1, 0));
	sun->setRevolutionSpeed(0); 
	sun->setLocalRotationSpeed(1);

	mercury = new Planet(cubeNode);
	mercury->createPlanet(mSceneMgr, 3, ColourValue::ColourValue(0.82f, 0.7f, 0.54));
	mercury->setParent(sun); mercury->getNode()->setPosition(mercury->getParent()->getNode()->getPosition().x + 100, 0, 0);

	mercury->setRevolutionSpeed(0.24); // 88 / 365
	mercury->setLocalRotationSpeed(1);	

	venus = new Planet(cubeNode);
	venus->createPlanet(mSceneMgr, 5, ColourValue::ColourValue(0.93, 0.9f, 0.67f));
	venus->setParent(sun); venus->getNode()->setPosition(venus->getParent()->getNode()->getPosition().x + 150, 0, 0);
	venus->setRevolutionSpeed(0.61); // 224 / 365
	venus->setLocalRotationSpeed(1);

	earth = new Planet(cubeNode);
	earth->createPlanet(mSceneMgr, 10, ColourValue::Blue);
	earth->setParent(sun); earth->getNode()->setPosition(earth->getParent()->getNode()->getPosition().x + 200, 0, 0);
	earth->setRevolutionSpeed(1);
	earth->setLocalRotationSpeed(1);

	moon = new Planet(cubeNode);
	moon->createPlanet(mSceneMgr, 1, ColourValue::ColourValue(0.7f, 0.7f, 0.7f));
	moon->setParent(earth); moon->getNode()->setPosition(moon->getParent()->getNode()->getPosition().x + 180, 0, 0);
	moon->setRevolutionSpeed(5);
	moon->setLocalRotationSpeed(1);

	mars = new Planet(cubeNode);
	mars->createPlanet(mSceneMgr, 8, ColourValue::ColourValue(0.71f, 0.25f, 0.05f));
	mars->setParent(sun); mars->getNode()->setPosition(mars->getParent()->getNode()->getPosition().x + 250, 0, 0);
	mars->setRevolutionSpeed(1.88); // 687 / 365
	mars->setLocalRotationSpeed(1);

	//mSceneMgr->getRootSceneNode()->createChildSceneNode()->attachObject(object);
}

bool TutorialApplication::frameStarted(const FrameEvent& evt)
{
	sun->update(evt);
	mercury->update(evt);
	venus->update(evt);
	earth->update(evt);
	moon->update(evt);
	mars->update(evt);

	return true;
}

//if (mKeyboard->isKeyDown(OIS::KC_I))
	//{
	//	cubeNode->translate(0, 0, (-cubeMovement) * evt.timeSinceLastFrame);	//FORWARD
	//	cubeMovement = cubeMovement + 60 * evt.timeSinceLastFrame;
	//}

	//if (mKeyboard->isKeyDown(OIS::KC_K))
	//{
	//	cubeNode->translate(0, 0, (cubeMovement)* evt.timeSinceLastFrame);		//BACKWARD
	//	cubeMovement = cubeMovement + 60 * evt.timeSinceLastFrame;
	//}

	//if (mKeyboard->isKeyDown(OIS::KC_J))
	//{
	//	cubeNode->translate((-cubeMovement) * evt.timeSinceLastFrame, 0, 0);	//LEFT
	//	cubeMovement = cubeMovement + 60 * evt.timeSinceLastFrame;
	//}

	//if (mKeyboard->isKeyDown(OIS::KC_L))
	//{
	//	cubeNode->translate((cubeMovement)* evt.timeSinceLastFrame, 0, 0);		//RIGHT
	//	cubeMovement = cubeMovement + 60 * evt.timeSinceLastFrame;
	//}

	/*movement *= evt.timeSinceLastFrame;
	mObject->translate(movement);*/

	/*if (mKeyboard->isKeyDown(OIS::KC_NUMPAD4))
	{
		Degree rotation = Degree(45 * evt.timeSinceLastFrame);
		mObject->rotate(Vector3(0, 1, 0), Radian(rotation));
	}
	*/

//---------------------------------------------------------------------------

#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
#define WIN32_LEAN_AND_MEAN
#include "windows.h"
#endif

#ifdef __cplusplus
extern "C" {
#endif

#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
	INT WINAPI WinMain(HINSTANCE hInst, HINSTANCE, LPSTR strCmdLine, INT)
#else
	int main(int argc, char *argv[])
#endif
	{
		// Create application object
		TutorialApplication app;

		try {
			app.go();
		}
		catch (Ogre::Exception& e) {
#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
			MessageBox(NULL, e.getFullDescription().c_str(), "An exception has occurred!", MB_OK | MB_ICONERROR | MB_TASKMODAL);
#else
			std::cerr << "An exception has occurred: " <<
				e.getFullDescription().c_str() << std::endl;
#endif
		}

		return 0;
	}

#ifdef __cplusplus
}
#endif

//---------------------------------------------------------------------------
