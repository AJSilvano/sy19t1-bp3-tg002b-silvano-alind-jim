#pragma once
#include <string>
using namespace std;
class Weapon
{
public:
	Weapon();
	~Weapon();

	int getDamage();
	string getName();

private:
	int mDamage;
	string mName;
};

