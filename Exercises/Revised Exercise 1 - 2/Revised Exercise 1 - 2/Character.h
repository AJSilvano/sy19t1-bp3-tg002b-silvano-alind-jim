#pragma once
#include <string>
#include "Weapon.h"
#include <vector>

using namespace std;

class Skill;

class Character
{
public:
	Character();
	~Character();

	void playerSkillCast(Character* target);
	Weapon* getWeapon();
	void reduceHp(int value);
private:
	Weapon* weapon;
	vector<Skill*> playerSkills;
	int hp;
	int mp;
	string name;
};

