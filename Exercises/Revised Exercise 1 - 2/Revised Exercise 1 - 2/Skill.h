#pragma once
#include <iostream>
#include <string>
#include "Character.h"
using namespace std;
class Skill
{
public:
	Skill();
	~Skill();

	void activate(Character* caster, Character *target);

protected:
	int mMpCost;
	string mName;
};

