#pragma once

#include <iostream>
#include <string>

using namespace std;

class playerSkill
{
public:
	void playerNormalAttack(string player, string enemy, int enemyHp, int mp, int weaponDamage);
	void playerCriticalAttack(string player, string enemy, int enemyHp, int mp, int weaponDamage);
};
