#pragma once

#include <iostream>
#include <string>

using namespace std;

class characterClass
{
public:
	characterClass();
	characterClass(string name, int hp, int mp, string weaponName, int weaponDamage);
	void displayStats();
	void attack(characterClass* enemy);
	void playerSkillUse(characterClass * enemy);
	bool dead();
private:
	string name;
	int hp;
	int mp;
	string weaponName;
	int weaponDamage;
	playerSkill *normalAttack = new playerSkill;
	playerSkill *criticalAttack = new playerSkill;
};