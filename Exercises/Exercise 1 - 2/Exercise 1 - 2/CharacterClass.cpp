#include "CharacterClass.h"
#include "Skill.h"
#include <time.h>

characterClass::characterClass()
{
	this->name = "";
	this->hp = 0;
	this->mp = 0;
	this->weaponName = "";
	this->weaponDamage = 0;
}

characterClass::characterClass(string name, int hp, int mp, string weaponName, int weaponDamage)
{
	this->name = name;
	this->hp = hp;
	this->mp = mp;
	this->weaponName = weaponName;
	this->weaponDamage = weaponDamage;
}

void characterClass::displayStats()
{
	cout << "[" << this->name << "]" << endl;
	cout << endl;
	cout << "Health: " << this->hp << endl;
	cout << "Mana: " << this->mp << endl;
	cout << "Current Weapon: " << this->weaponName << " | Damage: " << this->weaponDamage << endl;
}

void characterClass::attack(characterClass * enemy)
{
	int battleSkillChance = rand() % 100 + 1;

	if (battleSkillChance <= 20)
	{
		cout << this->name << " does a Normal Attack to " << enemy->name << endl;
		cout << enemy->name << " took " << this->weaponDamage << " damage!" << endl;
		enemy->hp -= this->weaponDamage;
		cout << enemy->name << " has " << enemy->hp << " hp left." << endl;
		system("pause");
	}

	else if (battleSkillChance > 20 && battleSkillChance < 40)
	{
		this->mp -= 6;
		if (this->mp < 6)
		{
			cout << "Your MP is insufficient to use this skill!" << endl;
		}
		cout << this->name << " does a Critical Attack to " << enemy->name << endl;
		cout << enemy->name << " took " << this->weaponDamage * 2 << " damage!" << endl;
		enemy->hp -= this->weaponDamage * 2;
		cout << enemy->name << " has " << enemy->hp << " hp left." << endl;
		system("pause");
	}

	else if (battleSkillChance > 40 && battleSkillChance < 60)
	{
		this->mp -= 7;
		if (this->mp < 7)
		{
			cout << "Your MP is insufficient to use this skill!" << endl;
		}
		cout << this->name << " does a Syphon Attack to " << enemy->name << endl;
		cout << enemy->name << " took " << this->weaponDamage << " damage!" << endl;
		cout << this->name << " heal for " << weaponDamage * 0.25 << " hp." << endl;
		enemy->hp -= this->weaponDamage;
		this->hp += this->weaponDamage * 0.25;
		cout << enemy->name << " has " << enemy->hp << " hp left." << endl;
		system("pause");
	}

	else if (battleSkillChance > 60 && battleSkillChance < 80)
	{
		this->mp -= 10;
		if (this->mp < 10)
		{
			cout << "Your MP is insufficient to use this skill!" << endl;
		}
		cout << this->name << " does a Vengeance Attack to " << enemy->name << endl;
		cout << this->name << " uses " << this->hp * 0.25 << " hp " << " to deal " << this->weaponDamage * 2 << " damage!" << endl;
		enemy->hp -= this->weaponDamage * 2;
		this->hp -= this->hp * 0.25;
		cout << this->name << " has " << enemy->hp << " hp left." << endl;
		if (this->hp <= 1)
		{
			this->hp = 1;
		}
		system("pause");
	}

	else if (battleSkillChance > 80 && battleSkillChance < 100)
	{
		this->mp -= 8;
		if (this->mp < 8)
		{
			cout << "Your MP is insufficient to use this skill!" << endl;
		}
		cout << this->name << " does a Dopel Blade Attack to " << enemy->name << endl;
		cout << this->name << " spawn a clone to use another skill to attack!" << endl;

		int dopelSkillChance = rand() % 100 + 1;
		if (dopelSkillChance <= 20)
		{
			cout << this->name << " does a Normal Attack to " << enemy->name << endl;
			cout << enemy->name << " took " << this->weaponDamage << " damage!" << endl;
			enemy->hp -= this->weaponDamage;
			cout << enemy->name << " has " << enemy->hp << " hp left." << endl;
			system("pause");
		}

		else if (dopelSkillChance > 20 && dopelSkillChance < 40)
		{
			this->mp -= 6;
			if (this->mp < 6)
			{
				cout << "Your MP is insufficient to use this skill!" << endl;
			}
			cout << this->name << " does a Critical Attack to " << enemy->name << endl;
			cout << enemy->name << " took " << this->weaponDamage * 2 << " damage!" << endl;
			enemy->hp -= this->weaponDamage * 2;
			cout << enemy->name << " has " << enemy->hp << " hp left." << endl;
			system("pause");
		}

		else if (dopelSkillChance > 40 && dopelSkillChance < 60)
		{
			this->mp -= 7;
			if (this->mp < 7)
			{
				cout << "Your MP is insufficient to use this skill!" << endl;
			}
			cout << this->name << " does a Syphon Attack to " << enemy->name << endl;
			cout << enemy->name << " took " << this->weaponDamage << " damage!" << endl;
			cout << this->name << " heal for " << weaponDamage * 0.25 << " hp." << endl;
			enemy->hp -= this->weaponDamage;
			this->hp += this->weaponDamage * 0.25;
			cout << enemy->name << " has " << enemy->hp << " hp left." << endl;
			system("pause");
		}

		else if (dopelSkillChance > 60 && dopelSkillChance < 80)
		{
			this->mp -= 10;
			if (this->mp < 10)
			{
				cout << "Your MP is insufficient to use this skill!" << endl;
			}
			cout << this->name << " does a Vengeance Attack to " << enemy->name << endl;
			cout << this->name << " uses " << this->hp * 0.25 << " hp " << " to deal " << this->weaponDamage * 2 << " damage!" << endl;
			enemy->hp -= this->weaponDamage * 2;
			this->hp -= this->hp * 0.25;
			cout << this->name << " has " << enemy->hp << " hp left." << endl;
			if (this->hp <= 1)
			{
				this->hp = 1;
			}
			system("pause");
		}

		else if (dopelSkillChance > 80 && dopelSkillChance < 100)
		{
			this->mp -= 8;
			if (this->mp < 8)
			{
				cout << "Your MP is insufficient to use this skill!" << endl;
			}
			cout << this->name << " does a Dopel Blade Attack to " << enemy->name << endl;
			cout << this->name << " spawn a clone to use another skill to attack!" << endl;
			system("pause");
		}
	}
}

void characterClass::playerSkillUse(characterClass * enemy)
{
	srand(time(NULL));
	if (this->mp > 9)
	{
		int skillRandomChance = rand() % 5 + 1;

		if (skillRandomChance == 1)
		{
			this->normalAttack->playerNormalAttack(this->name, enemy->name, weaponDamage, enemy->hp, this->mp);
		}
	}
}

bool characterClass::dead()
{
	return hp <= 0;
}
