#include "Skill.h"

void playerSkill::playerNormalAttack(string player, string enemy, int enemyHp, int mp, int weaponDamage)
{
	cout << player << " does a Normal Attack to " << enemy << endl;
	cout << enemy << " took " << weaponDamage << " damage!" << endl;
	enemyHp -= weaponDamage;
	cout << enemy << " has " << enemy << " hp left." << endl;
	system("pause");
}

void playerSkill::playerCriticalAttack(string player, string enemy, int enemyHp, int mp, int weaponDamage)
{
	cout << player << " does a Critical Attack to " << enemy << endl;
	cout << enemy << " took " << weaponDamage * 2 << " damage!" << endl;
	enemyHp -= weaponDamage * 2;
	cout << enemy << " has " << enemy << " hp left." << endl;
	system("pause");	
}
