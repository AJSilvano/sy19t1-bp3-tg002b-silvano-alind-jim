#include <iostream>
#include <string>
#include <time.h>
#include "CharacterClass.h"
#include <vector>

using namespace std;

int main()
{
	srand(time(NULL));
	int playerChoice = 0;
	vector<characterClass*> playerClass;
	vector<characterClass*> enemy;

	{
		characterClass *enemyPlayer = new characterClass ("The Evil One", 100, 75, "The Evil Sword", 8);
		enemy.push_back(enemyPlayer);
	}

	cout << "Hello Player! Please choose your desired class below: " << endl;
	cout << endl;
	cout << "1 - Warrior" << endl;
	cout << "2 - Thief" << endl;
	cout << "3 - Tank" << endl;

	cout << endl;
	cout << "Please input your desired class: ";
	cin >> playerChoice;

	cout << endl;

	if (playerChoice == 1)
	{
		characterClass *Warrior = new characterClass ("Warrior", 120, 80, "Long Sword", 10);
		playerClass.push_back(Warrior);
		cout << "You choose the warrior class!" << endl;
	}

	else if (playerChoice == 2)
	{
		characterClass *Thief = new characterClass("Thief", 100, 90, "Dagger", 8);
		playerClass.push_back(Thief);
		cout << "You choose the thief class!" << endl;
	}

	else if (playerChoice == 3)
	{
		characterClass *Tank = new characterClass("Tank", 160, 70, "Giant Sword", 7);
		playerClass.push_back(Tank);
		cout << "You choose the Tank class!" << endl;
	}

	system("pause");
	system("cls");

	cout << "LET THE BATTLE BEGIN!" << endl;
	system("pause");
	system("cls");

	while (true)
	{
		int skillChance = rand() % 100 + 1;
		int i = 0;
		playerClass[i]->displayStats();
		cout << endl;
		enemy[i]->displayStats();

		system("pause");
		system("cls");
		while (true)
		{
			int i = 0;
			playerClass[i]->attack(enemy[i]);
			cout << endl;
			enemy[i]->attack(playerClass[i]);
			cout << endl;
			system("cls");

			playerClass[i]->displayStats();
			cout << endl;
			enemy[i]->displayStats();
			system("pause");
			system("cls");

			if (playerClass[i]->dead())
			{
				cout << "The Evil One killed you on a deadly duel!" << endl;
				cout << "He's now corrupting everyone with Evil" << endl;
				system("pause");
				system("cls");
				break;
			}

			else if (enemy[i]->dead())
			{
				cout << "You won the life and death Duel!" << endl;
				cout << "You save the world from The Evil One!" << endl;
				system("pause");
				system("cls");
				break;
			}

			else if (enemy[i]->dead() && playerClass[i]->dead())
			{
				cout << "The Legend lost his life to save the world!" << endl;
				cout << "He defeated the Almight Evil One!" << endl;
				system("pause");
				system("cls");
				break;
			}
		}

		break;
	}

	cout << "Thank you for playing :)" << endl;
	system("pause");
}