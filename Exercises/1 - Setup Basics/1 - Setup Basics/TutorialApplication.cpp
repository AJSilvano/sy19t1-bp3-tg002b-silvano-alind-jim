/*
-----------------------------------------------------------------------------
Filename:    TutorialApplication.cpp
-----------------------------------------------------------------------------

This source file is part of the
   ___                 __    __ _ _    _
  /___\__ _ _ __ ___  / / /\ \ (_) | _(_)
 //  // _` | '__/ _ \ \ \/  \/ / | |/ / |
/ \_// (_| | | |  __/  \  /\  /| |   <| |
\___/ \__, |_|  \___|   \/  \/ |_|_|\_\_|
	  |___/
Tutorial Framework (for Ogre 1.9)
http://www.ogre3d.org/wiki/
-----------------------------------------------------------------------------
*/

#include "TutorialApplication.h"
#include <OgreManualObject.h>


//---------------------------------------------------------------------------
TutorialApplication::TutorialApplication(void)
{

}
//---------------------------------------------------------------------------
TutorialApplication::~TutorialApplication(void)
{
}

//---------------------------------------------------------------------------
void TutorialApplication::createScene(void)
{
	// Create your scene here :)

	// First you have to create an object in the scene
	ManualObject* object = mSceneMgr->createManualObject(); // don't do new ManualObject()

	// Begin drawing the object
	object->begin("BaseWhiteNoLighting", RenderOperation::OT_TRIANGLE_LIST);

	// Drawing code goes here
	// WE DRAW OUR TRIANGLE HERE

	//FRONT T-1
	object->position(-10, 10, 0);
	object->position(-10, 0, 0);
	object->position(0, 0, 0);

	//FRONT T-2
	object->position(0, 10, 0);
	object->position(-10, 10, 0);
	object->position(0, 0, 0);

	//LEFT T-1
	object->position(-10, 10, -10);
	object->position(-10, 0, -10);
	object->position(-10, 0, 0);

	//LEFT T-2
	object->position(-10, 10, 0);
	object->position(-10, 10, -10);
	object->position(-10, 0, 0);

	//RIGHT T-1
	object->position(0, 10, -10);
	object->position(0, 0, 0);
	object->position(0, 0, -10);

	//RIGHT T-2
	object->position(0, 10, 0);
	object->position(0, 0, 0);
	object->position(0, 10, -10);

	//BACK T-1
	object->position(-10, 10, -10);
	object->position(0, 0, -10);
	object->position(-10, 0, -10);

	//BACK T-2
	object->position(0, 10, -10);
	object->position(0, 0, -10);
	object->position(-10, 10, -10);

	//TOP T-1
	object->position(0, 10, -10);
	object->position(-10, 10, -10);
	object->position(-10, 10, 0);

	//TOP T-2
	object->position(0, 10, -10);
	object->position(-10, 10, 0);
	object->position(0, 10, 0);

	//BOTTOM T-1
	object->position(0, 0, -10);
	object->position(0, 0, 0);
	object->position(-10, 0, -10);

	//BOTTOM T-1
	object->position(-10, 0, -10);
	object->position(0, 0, 0);
	object->position(-10, 0, 0);

	// After drawing, you need to END the drawing
	object->end();
	// After ending the drawing, you need to add the object to the scene
	//mSceneMgr->getRootSceneNode()->createChildSceneNode()->attachObject(object);
	cubeNode = mSceneMgr->getRootSceneNode()->createChildSceneNode();
	cubeNode->attachObject(object);
}

bool TutorialApplication::keyPressed(const OIS::KeyEvent &arg)
{
	if (arg.key == OIS::KC_I)
	{
		cubeNode->translate(0, 0, -1);	//FORWARD ( I )
	}

	if (arg.key == OIS::KC_K)
	{
		cubeNode->translate(0, 0, 1);	//BACKWARDS ( K )
	}

	if (arg.key == OIS::KC_J)
	{
		cubeNode->translate(-1, 0, 0);	//LEFT ( J )
	}

	if (arg.key == OIS::KC_L)
	{
		cubeNode->translate(1, 0, 0);	//RIGHT ( L )
	}

	//else if (arg.key == OIS::KC_I && arg.key == OIS::KC_L)
	//{
	//	cubeNode->translate(1, 0, -1);	//North East
	//}

	//else if (arg.key == OIS::KC_K && arg.key == OIS::KC_L)
	//{
	//	cubeNode->translate(1, 0, 1);	//South East
	//}

	//else if (arg.key == OIS::KC_I && arg.key == OIS::KC_J)
	//{
	//	cubeNode->translate(-1, 0, -1);	//North West
	//}

	//else if (arg.key == OIS::KC_K && arg.key == OIS::KC_J)
	//{
	//	cubeNode->translate(-1, 0, 1);	//South West
	//}

	return true;
}

int cubeMovement = 60;

bool TutorialApplication::frameStarted(const FrameEvent& evt)
{
	if (mKeyboard->isKeyDown(OIS::KC_I))
	{
		cubeNode->translate(0, 0, (-cubeMovement) * evt.timeSinceLastFrame);	//FORWARD
		cubeMovement = cubeMovement + 60 * evt.timeSinceLastFrame;
	}

	if (mKeyboard->isKeyDown(OIS::KC_K))
	{
		cubeNode->translate(0, 0, (cubeMovement)* evt.timeSinceLastFrame);		//BACKWARD
		cubeMovement = cubeMovement + 60 * evt.timeSinceLastFrame;
	}

	if (mKeyboard->isKeyDown(OIS::KC_J))
	{
		cubeNode->translate((-cubeMovement) * evt.timeSinceLastFrame, 0, 0);	//LEFT
		cubeMovement = cubeMovement + 60 * evt.timeSinceLastFrame;
	}

	if (mKeyboard->isKeyDown(OIS::KC_L))
	{
		cubeNode->translate((cubeMovement)* evt.timeSinceLastFrame, 0, 0);		//RIGHT
		cubeMovement = cubeMovement + 60 * evt.timeSinceLastFrame;
	}

	//if (mKeyboard->isKeyDown(OIS::KC_I))
	//{
	//	if (mKeyboard->isKeyDown(OIS::KC_L) )
	//	cubeNode->translate(1, 0, -1); //North East
	//}

	//else if (mKeyboard->isKeyDown(OIS::KC_K) && mKeyboard->isKeyDown(OIS::KC_L))
	//{
	//	cubeNode->translate(1, 0, 1);	//South East
	//}

	//else if (mKeyboard->isKeyDown(OIS::KC_I) && mKeyboard->isKeyDown(OIS::KC_J))
	//{
	//	cubeNode->translate(-1, 0, -1);	//North West
	//}

	//else if (mKeyboard->isKeyDown(OIS::KC_K) && mKeyboard->isKeyDown(OIS::KC_J))
	//{
	//	cubeNode->translate(-1, 0, 1);	//South West
	//}

	//evt.timeSinceLastFrame;
	return true;
	//evt.timeSinceLastFrame;
}


//---------------------------------------------------------------------------

#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
#define WIN32_LEAN_AND_MEAN
#include "windows.h"
#endif

#ifdef __cplusplus
extern "C" {
#endif

#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
	INT WINAPI WinMain(HINSTANCE hInst, HINSTANCE, LPSTR strCmdLine, INT)
#else
	int main(int argc, char *argv[])
#endif
	{
		// Create application object
		TutorialApplication app;

		try {
			app.go();
		}
		catch (Ogre::Exception& e) {
#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
			MessageBox(NULL, e.getFullDescription().c_str(), "An exception has occurred!", MB_OK | MB_ICONERROR | MB_TASKMODAL);
#else
			std::cerr << "An exception has occurred: " <<
				e.getFullDescription().c_str() << std::endl;
#endif
		}

		return 0;
	}

#ifdef __cplusplus
}
#endif

//---------------------------------------------------------------------------
